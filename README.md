# **Shajeda Akter Moni**

![OmaKuva] (src/OmaKuva.jpg "Title")

## Profiili

Olen tehnyt monipuolista työtä Bangladeshissa. Suomeen saapumisen jälkeen uran vaihto oli alusta asti paras vaihtoehto, joten opiskelen tietojenkäsittelyksi Karelia (AMK):ssa. Ohjelmoijana työ on paras vaihtoehto. Työ antaa minulle mahdollisuuden työskennellä monipuolisesti kotona sekä toimistossa. Ohjelmointi, jolla on monipuoliset ominaispiirteet, on minulle mielenkiintoinen, koska se muuttuu jatkuvasti maailman kanssa. Tätä varten ahkera, luova, rehellinen ja nopea oppija voisi olla hyvä ominaisuus. Minä olen sellainen.

##   Työkokemus

### Times International Ltd. BD
 Virkailija, 15/05/2016 – 31/12/2016
Työnkuvaani kuului johtamiseen liittyviä tehtäviä. Vastuulleni kuului yhteyden pito ulkomaalaisiin yrityksiin. ostaaksesi tuotetta yrityksellemme. Vastuulleni kuului huolehtia myös muiden työntekijöiden suorituksista projektien mukaisesti.

###  Joensuun Kaupunki
Hyvinvointipalvelut, 04/12/2017 - 18/01/2016
Työssäoppimisjaksolla olen tutustunut suomalaiseen varhaiskasvatus- ja esiopetustyöskentelyyn.
 
## Koulutus

### Tietojekäsittely(Tradenomi)
**Karelia AmmattiKorkeakoulu** 24/08/2020 - Jatkuu

Tähän asti opiskeluni sujui hyvin. Tiedonhallinta ja SQL, Java, Käyttöliittymä ohjelmointi, Ohjelmisto tuotanto, Testausmenetelmä ja ym. ovat tähän mennessä merkittävät aiheet

### Sanskrit Kieli ja Kirjallisuus
University of Dhaka, Bangladesh.

opiskelin päätoimisesti sanskrit kieltä ja kirjallisuutta. Sen lisäksi kauppatiede, tietojenkäsittelytiede, filosofia, englanti ja ym. Olen suorittanut kandidaatti ja maisteri tutkinnot tällä alalla. 

## Harrastukset

Uinti, kävely, lukeminen, matkailu

## Kielitaito

Bangla, Suomi, Englanti

## Suosittelijat
 
**Erfan Ahmed**
Ohjelmoistokehittäjä, Keypro Oy
04000883990
erfanahmed1990@gmail.com


# Sää

![Sää] (src/sää.png "SÄÄ")
[Sää](https://www.ilmatieteenlaitos.fi/saa/jyv%C3%A4skyl%C3%A4)

# Syyslukukausi Opinnot KAMK

|     Tunnus 	|    Ryhmät 	|   Opintojakso     			          | Laajuus  |     Lukukausi      |
|---------------|---------------|-----------------------------------------|----------|--------------------|
|LTD6042-3002   |   LTDNS20     |[ICT-käyttäjätuki- ja koulutusosaaminen](https://www.ilmatieteenlaitos.fi/saa/jyv%C3%A4skyl%C3%A4)| 5        |      2021		  |


